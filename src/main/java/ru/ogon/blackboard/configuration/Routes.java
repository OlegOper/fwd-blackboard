package ru.ogon.blackboard.configuration;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Routes {
	private static final Map<String, String> routes;
	
	public static final String index = "/";
	public static final String home = "/home";
	public static final String login = "/login";
    public static final String logout = "/logout";
    public static final String admin = "/admin";
    public static final String db = "/db";
    public static final String access_denied = "/access_denied";
    
	public static final String board = "/board";
	public static final String board_details = board + "/";
	public static final String board_new = board + "/new";
	
	public static final String post = "/post";
	public static final String post_details = post + "/";
	public static final String post_new = post + "/new";
	
	public static final String comment = "/comment";
	public static final String comment_details = comment + "/";
	public static final String comment_new = comment + "/new";
	
    static {
    	Map<String, String> reflectedRoutes = new HashMap<>();
    	for (Field field : Routes.class.getDeclaredFields()) {
    	    if (java.lang.reflect.Modifier.isStatic(field.getModifiers())
    	    		&& field.getType().isAssignableFrom(String.class)) {
    	    	try {
    	    		reflectedRoutes.put(field.getName(),  (String) field.get(null));
				} catch (IllegalAccessException e) {
					throw new RuntimeException("Router setup failure.", e);
				}
    	    }
    	}
    	
    	routes = Collections.unmodifiableMap(reflectedRoutes);
    }
	
	public static Map<String, String> getRoutes()
    {
        return routes;
    }

    public static String getRoute(String dest)
    {
        return routes.get(dest);
    }
}
