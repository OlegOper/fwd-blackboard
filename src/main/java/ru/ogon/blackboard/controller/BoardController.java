package ru.ogon.blackboard.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ru.ogon.blackboard.configuration.Routes;
import ru.ogon.blackboard.model.Board;
import ru.ogon.blackboard.model.CustomUserDetails;
import ru.ogon.blackboard.service.BoardService;

@Controller
public class BoardController {
	
	@Autowired
    private BoardService boardService;

	@RequestMapping(value = Routes.board, method = RequestMethod.GET)
    public String boardList(ModelMap model) {
		model.addAttribute("routes", Routes.getRoutes());
        model.addAttribute("boardList", boardService.findAll());
        return "board_list";
    }
	
	@RequestMapping(value = Routes.board_details + "{id}", method = RequestMethod.GET)
    public String boardView(ModelMap model, @PathVariable(value="id") int id) {
		model.addAttribute("routes", Routes.getRoutes());
        model.addAttribute("board", boardService.findById(id));
        return "board_view";
    }
	
	//@PreAuthorize("hasRole('ADMIN')")
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = Routes.board_new, method = RequestMethod.GET)
    public String newBoardGet(ModelMap model) {
		model.addAttribute("routes", Routes.getRoutes());
        return "board_create_form";
    }
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = Routes.board_new, method = RequestMethod.POST)
	@Transactional
    public String newBoardPost(ModelMap model
    		, @AuthenticationPrincipal CustomUserDetails user
    		, @RequestParam("caption") String caption
    		, @RequestParam("description") String description) {
		Board board = new Board();
		board.setUser(user.getDomainModel());
		board.setCaption(caption);
		board.setDescription(description);
		board.setCreatedAt(LocalDateTime.now());
		boardService.persist(board);
        return "redirect:" + Routes.board;
    }
}
