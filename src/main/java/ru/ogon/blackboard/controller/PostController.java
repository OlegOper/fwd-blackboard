package ru.ogon.blackboard.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ru.ogon.blackboard.configuration.Routes;
import ru.ogon.blackboard.model.CustomUserDetails;
import ru.ogon.blackboard.model.Post;
import ru.ogon.blackboard.service.BoardService;
import ru.ogon.blackboard.service.PostService;

@Controller
public class PostController {
	
	@Autowired
    private PostService postService;
	
	@Autowired
    private BoardService boardService;
	
	@RequestMapping(value = Routes.post_details + "{id}", method = RequestMethod.GET)
    public String boardView(ModelMap model, @PathVariable(value="id") int id) {
		model.addAttribute("routes", Routes.getRoutes());
        model.addAttribute("post", postService.findById(id));
        return "post_view";
    }
	
	@RequestMapping(value = Routes.post_new, method = RequestMethod.GET)
    public String newPostGet(ModelMap model, @RequestParam(value="boardId") int boardId) {
		model.addAttribute("routes", Routes.getRoutes());
		model.addAttribute("boardId", boardId);
        return "post_create_form";
    }
	
	@RequestMapping(value = Routes.post_new, method = RequestMethod.POST)
	@Transactional
    public String newPostPost(ModelMap model
    		, @AuthenticationPrincipal CustomUserDetails user
    		, @RequestParam("caption") String caption
    		, @RequestParam("text") String text
    		, @RequestParam("boardId") int boardId) {
		model.addAttribute("routes", Routes.getRoutes());
		Post post = new Post();
		post.setBoard(boardService.findById(boardId));
		if (user != null) {
			post.setUser(user.getDomainModel());
		}
		post.setCaption(caption);
		post.setText(text);
		post.setCreatedAt(LocalDateTime.now());
		postService.persist(post);
        //return "redirect:" + Routes.board_details + boardId;
		return "redirect:" + Routes.post_details + post.getId();
    }
}
