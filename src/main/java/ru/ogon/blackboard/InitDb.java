package ru.ogon.blackboard;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import ru.ogon.blackboard.model.State;
import ru.ogon.blackboard.model.User;
import ru.ogon.blackboard.model.UserProfile;
import ru.ogon.blackboard.model.UserProfileType;

@SpringBootApplication
public class InitDb {

	public static void main(String[] args) throws Exception {		
		ApplicationContext ctx = SpringApplication.run(InitDb.class, args);

        System.out.println("Let's inspect the beans provided by Spring Boot:");

        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            System.out.println(beanName);
        }
        
        LocalSessionFactoryBean sessionFactory = ctx.getBean(LocalSessionFactoryBean.class);
        Session session = sessionFactory.getObject().openSession();
        try {
        	initDatabase(session);
        } finally {
        	session.close();
        }
	}
	
	protected static void initDatabase(Session session) {
	    Transaction txn = null;
	    try {
	        txn = session.beginTransaction();
	        
	        createUserProfiles(session);
	        createUsers(session);

	        txn.commit();
	    } finally {
	    	if (txn != null && txn.getStatus() != TransactionStatus.ACTIVE) {
	        	txn.rollback();
	        }
	    }
	}
	
	protected static void createUserProfiles(Session session) {
		for (UserProfileType userProfile : UserProfileType.values()) {
			UserProfile profile = new UserProfile();
			profile.setType(userProfile.getUserProfileType());
			session.persist(profile);
		}
	}
	
	protected static void createUsers(Session session) {
		User admin = new User();
		admin.setEmail("admin@admin.com");
		admin.setFirstName("ad");
		admin.setLastName("min");
		admin.setSsoId("admin");
		admin.setPassword("admin");
		admin.setState(State.ACTIVE.getState());
		
		@SuppressWarnings("unchecked")
		List<Object> userProfiles = session.createCriteria(UserProfile.class).list();
		Set<UserProfile> userProfilesSet = new HashSet<>();
		for (Object profile : userProfiles) {
			userProfilesSet.add((UserProfile)profile);
		}
		
		admin.setUserProfiles(userProfilesSet);
		session.persist(admin);
	}
}