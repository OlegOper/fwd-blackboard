package ru.ogon.blackboard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.ogon.blackboard.dao.CommentDao;
import ru.ogon.blackboard.model.Comment;

@Service("commentService")
@Transactional
public class CommentServiceImpl implements CommentService {
	
	@Autowired
    private CommentDao dao;

	@Override
	public Comment findById(int id) {
		return dao.findById(id);
	}

	@Override
	public List<Comment> findByPostId(int postId) {
		return dao.findByPostId(postId);
	}

	@Override
	public void persist(Comment comment) {
		dao.persist(comment);
	}

	@Override
	public void update(Comment comment) {
		dao.update(comment);
	}

	@Override
	public void delete(Comment comment) {
		dao.delete(comment);
	}
}
