package ru.ogon.blackboard.service;

import java.util.List;

import ru.ogon.blackboard.model.Comment;

public interface CommentService {
	
	Comment findById(int id);
	List<Comment> findByPostId(int postId);
	void persist(Comment comment);
	void update(Comment comment);
	void delete(Comment comment);
}
