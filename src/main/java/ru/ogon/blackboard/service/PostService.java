package ru.ogon.blackboard.service;

import java.util.List;

import ru.ogon.blackboard.model.Post;

public interface PostService {

	Post findById(int id);
	List<Post> findByBoardCaption(String boardCaption);
	void persist(Post post);
	void update(Post post);
	void delete(Post post);
}
