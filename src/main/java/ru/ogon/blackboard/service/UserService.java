package ru.ogon.blackboard.service;

import ru.ogon.blackboard.model.User;

public interface UserService {
	 
    User findById(int id);
     
    User findBySso(String sso);
     
}
