package ru.ogon.blackboard.service;

import java.util.List;

import ru.ogon.blackboard.model.Board;

public interface BoardService {

	Board findById(int id);
	List<Board> findAll();
	void persist(Board board);
	void update(Board board);
	void delete(Board board);
}
