package ru.ogon.blackboard.model;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="boards")
public class Board {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
	
	@Column(name = "created_at", columnDefinition = "timestamp", nullable=false)
    private LocalDateTime createdAt;
	
	@Column(name="caption", nullable=false)
	private String caption;
	
	@Column(name="description", nullable=false)
	private String description;
	
	@ManyToOne
    @JoinColumn(name = "user_id", nullable=false)
    private User user;
	
	@OneToMany(mappedBy="board", fetch=FetchType.EAGER)
    private Set<Post> posts;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}
}
