package ru.ogon.blackboard.dao;

import java.util.List;

import ru.ogon.blackboard.model.Board;

public interface BoardDao {
	
	Board findById(int id);
	List<Board> findAll();
	void persist(Board board);
	void update(Board board);
	void delete(Board board);
}
