package ru.ogon.blackboard.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ru.ogon.blackboard.model.Board;

@Repository("boardDao")
public class BoardDaoImpl extends AbstractDao<Integer, Board> implements BoardDao {

	@Override
	public Board findById(int id) {
		return getByKey(id);
	}

	@Override
	public List<Board> findAll() {
		return getAll();
	}
}
