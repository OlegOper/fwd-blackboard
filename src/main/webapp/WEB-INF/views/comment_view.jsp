<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:base>
  <jsp:attribute name="title">Comment <c:out value="${post.caption}" /></jsp:attribute>

  <jsp:body>
  	<h2><c:out value="${comment.caption}" /></h2>
    <div class="container"><c:out value="${comment.text}" /></div>
  	<a class="btn btn-primary" href="${routes.post_details}${comment.post.id}">Back</a>
  </jsp:body>
</t:base>