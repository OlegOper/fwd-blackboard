<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:base>
  <jsp:attribute name="title">New post</jsp:attribute>
  
  <jsp:body>
	<h1>New Post</h1>
	<form:form action="${routes.post_new}" method="POST">
	<label class="control-label col-sm-2">Post Caption:</label>
	<input class="form-control" type="text" name="caption">
	<br/>
	<label class="control-label col-sm-2">Text:</label>
	<textarea class="form-control" name="text"></textarea>
	<br/>
	<input type="hidden" name="boardId" value=${boardId} />
	<input class="btn btn-success" type="submit" value="Publish" />
	<a class="btn btn-danger" href="${routes.board_details}${boardId}">Cancel</a>
    </form:form>
  </jsp:body>
</t:base>
