<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:base>
  <jsp:attribute name="title">Post <c:out value="${post.caption}" /></jsp:attribute>

  <jsp:body>
    <a class="btn btn-default" href="${routes.board_details}${post.board.id}">
      <c:out value="${post.board.caption}"/>
    </a>
  	<h2><c:out value="${post.caption}" /></h2>
    <div class="container"><c:out value="${post.text}" /></div>
    
    
    <h3>Comments:</h3>
    
    <table class="table table-bordered">
      <c:forEach items="${post.comments}" var="comment">
      <tr>
        <td>
      	  <a href="${routes.comment_details}${comment.id}"><c:out value="${comment.caption}"/></a>
      	  <p style="font-size:0.7em"><c:out value="${comment.user.ssoId}"/></p>
      	  <p><c:out value="${comment.text}"/></p>
        </td>
      </tr>
      </c:forEach>
    </table>
    
    <form:form action="${routes.comment_new}" method="POST">
		<label class="control-label col-sm-2">Caption:</label>
		<input class="form-control" type="text" name="caption">
		<br/>
		<label class="control-label col-sm-2">Text:</label>
		<textarea class="form-control" name="text"></textarea>
		<br/>
		<input type="hidden" name="postId" value=${post.id} />
		<input class="btn btn-success" type="submit" value="Add comment" />
    </form:form>
      
  </jsp:body>
</t:base>