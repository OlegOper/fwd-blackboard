<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:base>
  <div>
    Greeting : ${greeting}
    This is a welcome page.
  </div>
  <ul class="list-group">
  <li class="list-group-item"><a href="${routes.board}">Boards</a></li>
  <li class="list-group-item"><a href="${routes.admin}">Admin tools</a></li>
  <li class="list-group-item"><a href="${routes.db}">DBA tools</a></li>
  <li class="list-group-item"><a href="${routes.login}">Login</a></li>
  <li class="list-group-item"><a href="${routes.logout}">Logout</a></li>
  </ul>
</t:base>
